import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Empleado } from '../models/empleado.model';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {


  private url = 'http://localhost:3000/api/empleados';
  constructor(private http: HttpClient) { }

  public getEmpleados(options?: any) {
    return this.http.get<Empleado[]>(this.url, options);
  }

  public crearEmpleado(body: any) {
    return this.http.post(this.url, body);
  }

  public actualizarEmpleado(empleado: Empleado) {
    const id = empleado._id;
    return this.http.put(`${this.url}/${id}`, empleado);
  }

  public eliminarEmpleado(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
}

import { Component, inject, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Empleado } from '../../models/empleado.model'
import { EmpleadoService } from '../../services/empleado.service'

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent {
  @ViewChild('crearEmpleadoForm') crearEmpleadoForm: any;
  hasErrors: boolean = false;
  empleados: Empleado[] = []
  empleado: any = {}

  http = inject(HttpClient);

  constructor(private empleadoService: EmpleadoService) { }


  editarEmpleado(empleado: Empleado) {
    this.empleado = empleado;
  }


  guardarNuevoEmpleado() {
    if (this.empleado._id) {
      const editarEmpleado = this.empleado
      this.empleadoService.actualizarEmpleado(editarEmpleado).subscribe((data: any) => {
        const { message } = data;
        alert(message);
        this.crearEmpleadoForm.reset();
        this.ngOnInit();
      })

    } else {
      const nuevoEmpleado = this.crearEmpleadoForm.value
      this.empleadoService.crearEmpleado(nuevoEmpleado).subscribe((data: any) => {
        const { message } = data;
        alert(message);
        this.crearEmpleadoForm.reset();
        this.ngOnInit();
      })
    }
  }

  enviarFormulario() {
    this.hasErrors = false;
    if (this.crearEmpleadoForm.valid) {
      this.guardarNuevoEmpleado();
    } else {
      this.hasErrors = true
    }
  }

  cancelarCreacion() {
    this.hasErrors = false
    this.crearEmpleadoForm.reset();
  }

  ngOnInit(): void {
    this.empleadoService.getEmpleados().subscribe((data: any) => {
      this.empleados = data.empleados;
    })
  }

  eliminarEmpleado(id: string) {
    this.empleadoService.eliminarEmpleado(id).subscribe((data: any) => {
      const { message } = data;
      alert(message);
      this.ngOnInit();
    })
  }
}
